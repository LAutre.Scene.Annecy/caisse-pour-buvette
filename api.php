<?php

/**
 * Simple example of extending the SQLite3 class and changing the __construct
 * parameters, then using the open method to initialize the DB.
 */
class MyDB extends SQLite3
{

    function __construct()
    {
        $this->open('stalls.db');
        $this->createTable();
    }

    public function createTable() {
        $commands = ['CREATE TABLE IF NOT EXISTS stalls(
            id INTEGER PRIMARY KEY,
            openDateTime DATETIME NOT NULL,
            closeDateTime DATETIME
        )', 'CREATE TABLE IF NOT EXISTS settings(
            id INTEGER PRIMARY KEY,
            name VARCHAR(20),
            minPrice DECIMAL (3,2),
            costPrice DECIMAL (3,2),
            supportPrice DECIMAL (3,2),
            stallId INTEGER,
            FOREIGN KEY (stallId)
            REFERENCES stalls(id) ON UPDATE CASCADE ON DELETE CASCADE
        )', 'CREATE TABLE IF NOT EXISTS payments(
            id INTEGER PRIMARY KEY,
            choice TEXT,
            minPrice DECIMAL (10,2),
            paidPrice DECIMAL (10,2),
            cashier VARCHAR(20),
            stallId INTEGER,
            FOREIGN KEY (stallId)
            REFERENCES stalls(id) ON UPDATE CASCADE ON DELETE CASCADE
        )'];
        foreach ($commands AS $command) {
            $this->exec($command);
        }
    }

    function createStall() {
        $date = new DateTime();
        $dateStr = $date->format('Y-m-d H:i:s');
        $query = "INSERT INTO stalls VALUES (NULL, '$dateStr', NULL)";
        $this->exec($query);
    }

    function checkStall($id) {
        $query = "SELECT * FROM stalls WHERE id = '$id' AND closeDateTime IS NULL";
        $command = $this->query($query);
        return $command->fetchArray(SQLITE3_ASSOC);
    }

    function checkIfOpenStallExist() {
        $query = "SELECT * FROM stalls WHERE closeDateTime IS NULL";
        $command = $this->query($query);
        return $command->fetchArray(SQLITE3_ASSOC);
    }

    function getLastStall() {
        $command = $this->query('SELECT id FROM stalls ORDER BY id DESC LIMIT 1');
        $return = $command->fetchArray(SQLITE3_ASSOC);
        return $return["id"];
    }

    function setSettings($json) {
        $stallId = $json->stallId;
        $result = $this->checkStall($stallId);
        if (!$result) {
            $this->createStall();
            $stallId = $this->getLastStall();
        }
        foreach($json->settings as $setting) {
            if ($setting->id <> 0 && $setting->name) {
                $query = "UPDATE settings SET name = '$setting->name',
                minPrice = '$setting->minPrice',
                costPrice = '$setting->costPrice',
                supportPrice = '$setting->supportPrice' WHERE id = '$setting->id'";
                $this->exec($query);
            } else if ($setting->id == 0 && $setting->name) {
                $query = "INSERT INTO settings VALUES (NULL, '$setting->name', '$setting->minPrice', '$setting->costPrice', '$setting->supportPrice', '$stallId')";
                $this->exec($query);
            } else if ($setting->id <> 0 && !$setting->name) {
                $query = "DELETE FROM settings WHERE id = '$setting->id'";
                $this->exec($query);
            }
        }
    }

    function getSettings($json) {
        $stallId = $json->stallId;
        $result = $this->checkIfOpenStallExist();
        if ($result) {
            if ($stallId <> 0) {
                $result = $this->checkStall($stallId);
            }
            $stallId = $result["id"];
        }
        if ($result) {
            $query = "SELECT * FROM settings WHERE stallId = '$stallId'";
            $command = $this->query($query);
            $echo = '{"key":"settings","values":[';
            while ($row = $command->fetchArray(SQLITE3_ASSOC)) {
                $echo .= json_encode($row).',';
            }
            $echo = trim($echo, ',');
            $echo .= ']}';
            return $echo;
        } else {
            $result = $this->getLastStall();
            return '{"key":"lastStallId","values":'.json_encode($result).'}';
        }
    }

    function closeStall($json) {
        $date = new DateTime();
        $dateStr = $date->format('Y-m-d H:i:s');
        $query = "UPDATE stalls SET closeDateTime = '$dateStr' WHERE id = '$json->stallId'";
        $this->exec($query);
    }
    
    function setPayment($json) {
        $query = "INSERT INTO payments VALUES (NULL, '$json->choices', '$json->minPrice', '$json->paidPrice', '$json->cashier', '$json->stallId')";
        $this->exec($query);
        $echo = $this->lastInsertRowID();
        return $echo;
    }

    function getCashiersByStall($id) {
        $query = "SELECT DISTINCT cashier FROM payments WHERE stallId = '$id'";
        $command = $this->query($query);
        $echo = '{"key":"cashiers","values":[';
        while ($row = $command->fetchArray(SQLITE3_ASSOC)) {
            $echo .= json_encode($row).',';
        }
        $echo = trim($echo, ',');
        $echo .= ']}';
        return $echo;
    }

    function getStalls($typeOfReturn = "JSON") {
        $query = "SELECT * FROM stalls ORDER BY id DESC";
        $command = $this->query($query);
        if ($typeOfReturn == "JSON") {
            $echo = '{"key":"stalls","values":[';
            while ($row = $command->fetchArray(SQLITE3_ASSOC)) {
                $echo .= json_encode($row).',';
            }
            $echo = trim($echo, ',');
            $echo .= ']}';
            return $echo;
        } else if ($typeOfReturn == "Array") {
            while ($row = $command->fetchArray(SQLITE3_ASSOC)) {
                $echo[] = [$row["id"], $row["openDateTime"], $row["closeDateTime"]];
            }
            return $echo;
        }
    }

    function getGeneralPayments() {
        $stalls = $this->getStalls("Array");
        $echo = '{"key":"payments","values":[';
        foreach ($stalls AS $stall) {
            $query = "SELECT * FROM payments WHERE stallId = '$stall[0]'";
            $command = $this->query($query);
            $totalMin = 0;
            $totalPaid = 0;
            while ($row = $command->fetchArray(SQLITE3_ASSOC)) {
                $totalMin = $totalMin + $row["minPrice"];
                $totalPaid = $totalPaid + $row["paidPrice"];
            }
            $totalPaid = $totalPaid - $totalMin;
            $echo .= '{"stallId":'.$stall[0].',"openDateTime":"'.$stall[1].'","closeDateTime":"'.$stall[2].'","minPrice":'.$totalMin.',"paidPrice":'.$totalPaid.'},';
        }
        $echo = trim($echo, ',');
        $echo .= ']}';
        return $echo;
    }

    function getChoicesByCashier($json) {
        $query = "SELECT id,choice FROM payments WHERE stallId = '$json->stallId' AND cashier = '$json->cashier' ORDER BY id DESC";
        $command = $this->query($query);
        $echo = '{"key":"choices","cashier":"'.$json->cashier.'","values":[';
        while ($row = $command->fetchArray(SQLITE3_ASSOC)) {
            $echo .= json_encode($row).',';
        }
        $echo = trim($echo, ',');
        $echo .= ']}';
        return $echo;
    }

    function getChoicesByStall($id) {
        $query = "SELECT id,choice FROM payments WHERE stallId = '$id' ORDER BY id DESC";
        $command = $this->query($query);
        $echo = '{"key":"choices","values":[';
        while ($row = $command->fetchArray(SQLITE3_ASSOC)) {
            $echo .= json_encode($row).',';
        }
        $echo = trim($echo, ',');
        $echo .= ']}';
        return $echo;
    }
}

$db = new MyDB();

header("Content-Type: application/json; charset=UTF-8");
$json = json_decode(file_get_contents('php://input'), false);
$command = "";
if ($json->command == "setSettings") {
    $db->setSettings($json);
    echo "OK";
} else if ($json->command == "getSettings") {
    echo $db->getSettings($json);
} else if ($json->command == "closeStall") {
    $db->closeStall($json);
    echo "OK";
} else if ($json->command == "setPayment") {
    echo $db->setPayment($json);
} else if ($json->command == "getStalls") {
    echo $db->getStalls();
} else if ($json->command == "getGeneralPayments") {
    echo $db->getGeneralPayments();
} else if ($json->command == "getChoicesByCashier") {
    echo $db->getChoicesByCashier($json);
} else if ($json->command == "getCashiersByStall") {
    echo $db->getCashiersByStall($json->stallId);
} else if ($json->command == "getChoicesByStall") {
    echo $db->getChoicesByStall($json->stallId);
}
?>