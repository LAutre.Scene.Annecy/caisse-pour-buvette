# Caisse pour buvette

Système de caisse pour faciliter la comptabilité d'une buvette à prix libre et fluidifier les buvettes de taille importante.
La personne qui prend la commande donne le numéro de commande au client, qui va voir le serveur après paiement pour lui donner ce même numéro. Le serveur lui sert la commande associée.

Le fichier index permet de préparer la buvette avec ce qui est vendu, et les tarifs associés. Il permet ensuite de prendre des commandes et de les envoyer au serveur.
Le fichier choices permet de récupérer toutes les 15 secondes les nouvelles commandes afin de pouvoir les servir.
Le fichier admin permet d'avoir une vue d'ensemble sur les buvettes et l'argent récolté.

## Format
Version faite principalement pour mobile.


## Licence

Licence CC by SA - L'Autre Scène 2024


## Contributions

Sentez-vous libre de contribuer comme bon vous semble. Ce développement fut rapide, il mérite certainement des améliorations !


## Questions

Pour toutes questions, contactez Casa sur contact@lautre-scene.fr
